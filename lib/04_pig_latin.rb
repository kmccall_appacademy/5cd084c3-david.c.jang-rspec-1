def translate(english)
  vowels = "aeiou"
  punctuations = ".,;:'\"!@<>?"
  words = english.split


  ans = words.map do |word|
    capital = false
    punctuation = false
    indx_of_vowel = 0

    capital = true if word[0] == word[0].upcase
    punctuation = true if punctuations.include?(word[-1])

    until vowels.include?(word[indx_of_vowel]) do
      indx_of_vowel+=1
    end
    indx_of_vowel += 1 if word[indx_of_vowel - 1] == "q"
    transform_to_proper_output(word,indx_of_vowel,capital,punctuation)


  end
  ans.join(" ")
end

def transform_to_proper_output(word, indx_of_vowel, capital, punctuation)
  last_indx = -1
  last_indx -= 1 if punctuation
  punctuation ? punct = word[-1] : punct = ""
  capital ? word[indx_of_vowel..last_indx].capitalize  + word[0...indx_of_vowel].downcase + "ay" + punct
            : word[indx_of_vowel..last_indx]  + word[0...indx_of_vowel] + "ay"  + punct

end
