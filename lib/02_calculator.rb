def add(param1, param2)
  param1 + param2
end

def subtract(param1, param2)
  param1 - param2
end

def sum(arr)
  return 0 if arr.empty?
  arr.reduce(:+)
end

def multiply(*p)
  p.reduce(:*)
end

def power(base,power)
  base**power
end

def factorial(n)
  return 1 if n == 0 || n == 1
  (1..n).to_a.reduce(:*)
end
