def echo(str)
  str
end

def shout(str)
  str.upcase
end

def repeat(str, repeat=2)
  output = str
  (repeat-1).times do
    output += " #{str}"
  end
  output

end

def start_of_word(str, length_to_return)
  str[0...length_to_return]
end

def first_word(sentence)
  sentence.split[0]
end

LITTLE_WORDS = ["and","over","the"]

def titleize(sentence)
  ans = sentence.split.map.with_index do |word,index|
    if LITTLE_WORDS.include?(word) && index != 0
      word
    else
      word.capitalize
    end
  end
  ans.join(' ')

end
